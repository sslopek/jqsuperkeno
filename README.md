jqSuperKeno
-----------
jqSuperKeno is a video keno game written in JavaScript utilizing jQuery. The project was started on 2013-07-11 by Steven Slopek.

Pay table based on http://wizardofodds.com/games/keno/



Future Changes
--------------
 * Update pay table values
 * Handling negative credit
 * ~~Improved sound effects~~
 * Visual display of numbers drawn
 * Display pay table for current count of picked numbers


Credits
-------
Sound effects by Kenney www.kenney.nl (licensed CC0)


License
-------
This software is released under the included MIT license.


